# 1. Registro e hospedagem

Existem diversas opções nesse quesito hoje em dia. Não entrarei muito em detalhes
sobre aquelas opções que não escolhi, portanto falarei aqui do Amazon Web Services
e do Registro.br, que foram os sites que me forneceram os serviços que precisava.

## 1.1 Amazon Web Services

Também chamado de AWS para encurtar, é o que fornece o serviço de hospedagem; isso é, o lugar onde está o computador em que o servidor está funcionando. Tecnicamente falando, não existe tal coisa como um computador para mim: é uma ou mais máquinas (tal qual um notebook ou um desktop, porém muito muito **muito** mais potente)  rodam, como programas, outros sistemas operacionais. E um desses sistemas é meu.

Também não existe tal coisa como "comprar" um computador desses. O preço que é cobrado é calculado com base no tempo de uso desse computador, como um aluguel. É necessário pagar mensalmente (ou anualmente) para manter ele rodando. Os principais fatores de cobrança são o poder de processamento do computador e o tempo que ele fica ativo. Um computador com 4 núcleos será mais caro que com apenas 1, 1GB de RAM é mais caro que 256MB e assim por diante. O mesmo vale para o tempo, mas não me aventurarei muito nessa parte: ainda não tenho experiências com precisar deixar o computador desligado. Eventualmente, falarei sobre o quanto meu servidor usa dessas capacidades e farei algumas observações.

Perto dos competidores, o AWS tem uma vantagem difícil de ignorar: após o registro, o usuário recebe um ano de uso gratuito de uma máquina virtual básica. Para o meu caso, "básica" foi muito mais do que suficiente.

Com essas variáveis em mente e com a configuração atual eu antecipo que meu custo será de 50US$/ano, aproximadamente (a partir de setembro do ano que vem). Isso dá cerca de 170R$/ano (ou 15R$/mês). Para um servidor de emails, parece um preço elevado. Porém, vale a pena observar (e eventualmente chegarei lá) que é possível colocar outros serviços nesse servidor, pois ele está *inteiramente* em nosso controle.

## 1.2 Registro.br

Um domínio é o nome seu servidor legível a humanos e acessível pela internet. Como *alexhertzog.com.br*, *facebook.com* ou *thepiratebay.org*. Ao digitar um nome de site na barra do navegador, o endereço IP do servidor é buscado na internet. Só depois que é feita essa resolução de nomes se torna possível acessar o site pelo nome. Vale a pena salientar que um domínio não é estritamente necessário. Todos sites são acessíveis pelos seus respectivos IPs (com possíveis exceções para subdomínios), como por exemplo: [31.13.65.36](31.13.65.36) é o IP do facebook; [172.217.29.67](172.217.29.67) é o IP da Google. Apenas é muito mais fácil dizer "alexhertzog.com.br" do que "cinquenta-e-quatro ponto setenta ponto trinta e dois ponto cento e noventa e seis" para alguém.

Existem muitas formas de registrar domínios hoje em dia. Eu pesquisei por cima e  sites fornecem domínios a cerca de 30US$/ano (mas tenho a impressão de que esse é preço dos mais caros) em domínios *.com*. [Mais informações sobre a hierarquia de domínios na internet (inglês)](https://en.wikipedia.org/wiki/Top-level_domain). Porém, já havia sido recomendado para mim o [registro.br](https://registro.br/), site nacional para registro de domínios *.br*, que inclui *.com.br*. Existem inúmeras opções além do *com*, ficando a gosto do freguês escolher o que preferir (pessoalmente, eu fiquei tentado em registrar um *.eng.br*, mas optei pelo mais comum). Só mantenha em mente que será necessário verbalizar esse endereço para alguém, o mais comum pode ser a melhor escolha.

Depois de registrado um domínio e uma breve configuração, um processo mágico espalha as novas informações e de repente a sua página responde no navegador. [Mais informações sobre propagação DNS (apenas inglês)](https://en.wikipedia.org/wiki/Domain_Name_System#Record_caching).

Registrar um domínio no Registro.br custa 30R$ por ano, ou 2,50R$ por mês. Aqui não existem descontinhos espertos para os marinheiros de primeira viagem.

## 1.3 Considerações finais

Como eu disse, existem muitos serviços para hospedagem e para registro de domínios. Apesar de eu ter usado esses dois, não foi uma escolha totalmente alienada: escolhi aqueles em que pessoas a minha volta já tiveram experiências. O meu conselho é escolher serviços que na hora do "puta merda" tu vai saber quem contatar para resolver as coisas, sem ser necessário recorrer a um suporte técnico.

Sei que alguns dos mais populares são [GoDaddy](https://br.godaddy.com/), [Cloudflare (apenas inglês)](https://www.cloudflare.com/). Também não tenho certeza dos serviços que eles provêm, mas creio que é tanto o registro quanto a hospedagem.

Para mim, 200R$/ano parece um ótimo investimento para tamanha diversidade de serviços e usos que se pode tirar disso. Atualmente, uso meu servidor para emails e sincronização de arquivos, contatos e calendário. Mas o céu é o limite. É possível utilizar leitores de feeds RSS (vários blogs de humor bombando), backup de arquivos (a tendência é que os dados tenham uma ótima durabilidade remotamente), [serviços de música pelo navegador](http://www.fomori.org/cherrymusic/)... E por aí vai.
